<?php

/** Configuration Variables **/

// if user features are being used, this loads the user from the session automatically
define('AUTOLOAD_USER', TRUE);

define('LOGIN_REDIRECT', 'channel-2');

define('PAGINATE_LIMIT', '50');
