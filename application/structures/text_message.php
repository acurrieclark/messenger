<?php

$structure = array(
	array('Text', 'smalltext'),
	array('Number', 'phone_number'),
	array('Sent', 'yesno', "default_value" => 'No'),
	array('Received', 'yesno', "default_value" => 'No'),
	array('message_id', 'string', 'not_required' => true),
	array('original_message_id', 'number', "not_required" => true),
	array('error_code', 'number', "not_required" => true),
	array('error_message', 'string', "not_required" => true),
	array('network', 'string', "not_required" => true),
	array('user_id', 'id', 'not_required' => true),
	array('cost', 'float', 'not_required' => true)
);

?>
