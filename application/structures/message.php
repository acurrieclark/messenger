<?php

$structure = array(
	array('Job Name', 'string'),
	array('Production Company', 'string'),
	array('Message', 'smallText'),
	array('Category', 'checkbox', array('model' => "message_category", 'field' => 'Title')),
	array('Production Type', 'drop', array('model' => "production_type", 'field' => 'Title')),
	array('Approved', 'yesno', 'default' => 'No'),
	array('Sent', 'yesno', 'default' => 'No'),
	array('Original Message', 'hidden', 'not_required' => true)
);

?>
