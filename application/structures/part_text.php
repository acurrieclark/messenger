<?php

$structure = array(
	array("msisdn", 'string'),
	array("to", 'string'),
	array("messageId", 'string'),
	array("concat_ref", 'number'),
	array("concat_total",'number'),
	array("concat_part", 'number'),
	array("text", 'smalltext'),
	array("type",'string'),
	array("message_timestamp", 'timestamp')
);

?>
