<?php

$structure = array(
	array('email', 'email', 'not_required' => true),
	array('Subject', 'smalltext', 'not_required' => true),
	array('Mobile Number', 'string', 'not_required' => true),
	array('User ID', 'number', 'not_required' => true),
	array('Source', 'drop', array('Mobile', 'email')),
	array('Content', 'text'),
	array('Read?', 'yesno', 'short_name' => 'read', 'default_value' => 'No'),
	array('Network', 'string', 'not_required' => true),
	array('message_id', 'string', 'not_required' => true)
);

?>
