<?php

/**
* Message model
*/
class message extends model
{

	static function remove_international_code($number) {
		return preg_replace("/^447/", "07", $number);
	}

	static function add_international_code($number) {
		return preg_replace("/^07/", "447", $number);
	}

}


?>
