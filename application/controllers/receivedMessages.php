<?php

/**
* Received Messages Controller
*/

class receivedMessagesController extends applicationController
{

	function __construct()
	{
		// Use this to control the creation of your application controller
		parent::__construct();
		$this->before_filters = array('must_be_admin' => array('except' => array('text')));

	}

	function _index() {
		$this->received_messages = received_message::find_all();
	}

	function _show() {
		$this->received_message = received_message::find_by_id($this->id);
		if ($this->request_type == 'ajax') {
			$returnable = new stdClass();
			$returnable->message = $this->received_message->values(array('all' => true));
			if (isset($this->received_message->UserID->value)) {
				$returnable->user = user::find_by_id($this->received_message->UserID)->values();
			}
			echo json_encode($returnable);
		}
	}

	function _new() {
		$this->received_message = new received_message($this->posted);
	}

	function _create() {
		$this->received_message = new received_message($this->posted);
		if ($this->received_message->save()) {
			flash('Received Message created.');
			redirect_to('received_messages');
		}
		else {
			error('There appears to be an error with the received_message.');
			render_action('new');
		}
	}

	function _mark_as_read() {
		must_be_ajax();
		$message = received_message::find_by_id($this->id);
		$message->read->value = "Yes";
		if ($message->update()) {
			flash('Success');
		}
	}

	function _mark_as_uread() {
		must_be_ajax();
		$message = received_message::find_by_id($this->id);
		$message->read->value = "No";
		if ($message->update()) {
			flash('Success');
		}
	}

	function _text() {
		// ?msisdn=447825346673&to=447507327622&messageId=03000000753B8EBC&text=Test&type=text&keyword=TEST&message-timestamp=2015-04-29+09%3A40%3A11&concat=true&concat-ref=194&concat-total=2&concat-part=2

		include_once(ROOT."/library/vendor/other/nexmo/NexmoMessage.php");

		$incoming = new NexmoMessage(NEXMO_KEY, NEXMO_SECRET);
		if ($incoming->inboundText()) {

			if (isset($this->get['concat'])) {
				// replace all hyphens in keys with underscores
				foreach($this->get as $key => $value) {
					if (strpos($key, '-') != null) {
						$this->get[str_replace('-', '_', $key)] = $this->get[$key];
						unset($this->get[$key]);
					}
				}
				$part_text = new part_text($this->get);
				if ($part_text->save()) {
					$matching_count = part_text::count(['where' => ['concat_ref' => $this->get['concat_ref']]]);
					if ($matching_count == $this->get['concat_total']) {
						$matching_parts = part_text::find_all(['where' => ['concat_ref' => $this->get['concat_ref']]]);
						foreach ($matching_parts as $part) {
							$final_text[$part->concat_part->value] = $part->text->value;
						}
						ksort($final_text);
						$text = implode('', $final_text);
						logger::Text_Message('Whole text formed');
						foreach ($matching_parts as $part) {
							$part->destroy();
						}
					}
					else {
						logger::Text_Message('Part text saved');
						logger::write();
						exit(header("Status: 200 OK"));
					}
				}
				else {
					error('part text not saved');
					logger::write();
					exit(header("Status: 200 OK"));
				}
			}
			else {
				$text = $incoming->text;
			}

			$text_details = array(
				"MobileNumber" => message::remove_international_code($incoming->from),
				"Content" => $text,
				"Source" => 'Mobile',
				"Network" => $incoming->network,
				"message_id" => $incoming->message_id
				);
			$user = user::find(array("where" => array("MobileNumber" => message::remove_international_code($incoming->from))));
			if ($user) {
				$text_details['UserID'] = $user->id->value;
			}
			$text = new received_message($text_details);
			if ($text->save()) {
				logger::write();
				exit(header("Status: 200 OK"));
			}
			else
				error('not saved');
		}
		else echo "no text message found";

	}

	// incoming email is dealt with in the mailhandler script

	function _edit() {
		$this->received_message = received_message::find_by_id($this->id);
	}

	function _update() {
		$this->received_message = new received_message(array_merge(array('id' => $this->id), $this->posted));
		if ($this->received_message->update()) {
			flash('Received Message updated.');
			redirect_to('received_messages');
		}
		else {
			error('There appears to be an error with the received_message.');
			render_action('edit');
		}
	}

		function _delete() {
			$this->received_message = received_message::find_by_id($this->id);
		if ($this->received_message->destroy()) {
			flash('Received Message '. $this->id .' deleted');
			redirect_to('back');
		}
		else {
			error('Received Message could not be deleted');
			redirect_to('back');
		}
	}


}

?>
