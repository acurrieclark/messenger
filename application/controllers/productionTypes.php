<?php

/**
* Production Types Controller
*/

class productionTypesController extends applicationController
{

	function __construct()
	{
		// Use this to control the creation of your application controller
		parent::__construct();
		$this->before_filters = array('must_be_admin' => array('all'));
	}

	function _index() {
		$this->production_types = production_type::find_all();
	}

	function _show() {
		$this->production_type = production_type::find_by_id($this->id);
	}

	function _new() {
		$this->production_type = new production_type($this->posted);
	}

	function _create() {
		$this->production_type = new production_type($this->posted);
		if ($this->production_type->save()) {
			flash('Production Type created.');
			redirect_to('production_types');
		}
		else {
			error('There appears to be an error with the production_type.');
			render_action('new');
		}
	}

	function _edit() {
		$this->production_type = production_type::find_by_id($this->id);
	}

	function _update() {
		$this->production_type = new production_type(array_merge(array('id' => $this->id), $this->posted));
		if ($this->production_type->update()) {
			flash('Production Type updated.');
			redirect_to('production_types');
		}
		else {
			error('There appears to be an error with the production_type.');
			render_action('edit');
		}
	}

		function _delete() {
			$this->production_type = production_type::find_by_id($this->id);
		if ($this->production_type->destroy()) {
			flash('Production Type '. $this->id .' deleted');
			redirect_to('back');
		}
		else {
			error('Production Type could not be deleted');
			redirect_to('back');
		}
	}


}

?>
