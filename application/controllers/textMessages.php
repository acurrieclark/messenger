<?php

/**
* Text Messages Controller
*/

class textMessagesController extends applicationController
{

	function __construct()
	{
		// Use this to control the creation of your application controller
		parent::__construct();
		$this->before_filters = array('must_be_admin' => array('all'));

	}

	function _index() {
		$this->text_messages = text_message::find_all();
	}

	function _show() {
		$this->text_message = text_message::find_by_id($this->id);
	}

	function _new() {
		$this->text_message = new text_message($this->posted);
	}

	function _create() {
		$this->text_message = new text_message($this->posted);
		if ($this->text_message->save()) {
			flash('Text Message created.');
			redirect_to('text_messages');
		}
		else {
			error('There appears to be an error with the Text Message.');
			render_action('new');
		}
	}

	function _edit() {
		$this->text_message = text_message::find_by_id($this->id);
	}

	function _update() {
		$this->text_message = new text_message(array_merge(array('id' => $this->id), $this->posted));
		if ($this->text_message->update()) {
			flash('Text Message updated.');
			redirect_to('text_messages');
		}
		else {
			error('There appears to be an error with the Text Message.');
			render_action('edit');
		}
	}

		function _delete() {
			$this->text_message = text_message::find_by_id($this->id);
		if ($this->text_message->destroy()) {
			flash('Text Message '. $this->id .' deleted');
			redirect_to('back');
		}
		else {
			error('Text Message could not be deleted');
			redirect_to('back');
		}
	}


}

?>
