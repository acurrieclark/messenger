<?php

/**
* Messages Controller
*/

class messagesController extends applicationController
{

	function __construct()
	{
		// Use this to control the creation of your application controller
		parent::__construct();
		$this->before_filters = array('must_be_admin' => array(
											'except' => array(
												'show'
												)
											),
										'must_be_logged_in' => array('all'));
	}

	function _index() {
		$this->messages = message::find_all();
	}

	function _sent() {
		$this->messages = message::find_all(array('where' => array('Sent' => 'Yes'), 'order' => array('created_at' => 'DESC')));
		if ($this->request_type == 'ajax' && !empty($this->messages)) {
			$returnable = new stdClass();
			foreach ($this->messages as $key => $message) {
				$returnable->$key = $message->values();
			}
			echo json_encode($returnable);
		}
	}

	function _unsent() {
		$this->messages = message::find_all(array('where' => array('Sent' => 'No'), 'order' => array('created_at' => 'DESC')));
		if ($this->request_type == 'ajax' && !empty($this->messages)) {
			$returnable = new stdClass();
			foreach ($this->messages as $key => $message) {
				$returnable->$key = $message->values();
			}
			echo json_encode($returnable);
		}
	}

	function _show() {
		$this->message = message::find_by_id($this->id);
		if ($this->request_type == 'ajax') {
			$returnable = new stdClass();
			$returnable->message = $this->message->values(array('all' => true));
			echo json_encode($returnable);
		}
	}

	function _new() {
		$this->set_menu('outbox');
		$this->message = new message($this->posted);
		if (isset($this->get['received'])) {
			$this->message->OriginalMessage->value = $this->get['received'];
			$this->received = received_message::find_by_id($this->get['received']);
		}
	}

	function _create() {
		$this->message = new message($this->posted);
		if (isset($this->get['received'])) {
			$this->received = received_message::find_by_id($this->get['received']);
		}
		$this->message->Sent->value = 'No';
		$this->message->Approved->value = 'No';

		if ($this->message->save()) {
			flash('Message created.');
			redirect_to('outbox');
		}
		else {
			error('There appears to be an error with the message.');
			render_action('new');
		}
	}

	function _edit() {
		$this->message = message::find_by_id($this->id);
		if ($this->message->OriginalMessage->value) {
			$this->received = received_message::find_by_id($this->message->OriginalMessage->value);
		}
	}

	function _update() {
		$this->message = new message(array_merge(array('id' => $this->id), $this->posted));
		if ($this->message->update()) {
			flash('Message updated.');
			redirect_to('outbox');
		}
		else {
			error('There appears to be an error with the message.');
			render_action('edit');
		}
	}

		function _delete() {
			$this->message = message::find_by_id($this->id);
		if ($this->message->destroy()) {
			flash('Message '. $this->id .' deleted');
			redirect_to('back');
		}
		else {
			error('Message could not be deleted');
			redirect_to('back');
		}
	}

	function _approve() {
		$message = message::find_by_id($this->id);
		if (!empty($message)) {
			$message->Approved->value = "Yes";
			if ($message->update()) {
				flash('Message approved');
			}
			else {
				error('Message could not be approved');
			}
		}
		else error('Message does not exist');
	}

	function _send() {

		must_be_ajax();

		$error_found = false;

		$texts_sent = 0;
		$emails_sent = 0;

		$this->message = message::find_by_id($this->id);
		if (empty($this->message)) error('Message not found');
		else if ($this->message->Sent->value == 'Yes') error('Message already sent');
		else if ($this->message->Approved->value == 'No') error('Message has not been approved');

		else {
			foreach (explode(", ", $this->message->Category) as $category) {
				$users = user::find_all(array('where' => array('message_types LIKE' => '%'.$category.'%')));
				if (!empty($users)) {
					foreach ($users as $key => $user) {
						if (strpos($user->contact_type, 'Text Message') !== false) {
							$texts[$user->full_name()] = $user->MobileNumber->value;
						}
						if (strpos($user->contact_type, 'email') !== false) {
							$emails[$user->full_name()] = $user->email->value;
						}
					}
				}
			}
			if (!empty($emails)) {
				foreach ($emails as $name => $email) {

					$mail_properties['mail_to'] = $email;
					$mail_properties['mail_from'] = MESSAGES_EMAIL_ADDRESS;
					$mail_properties['subject'] = $this->message->JobName." - ".$this->message->ProductionCompany;

					$mail_properties['plain'] = $this->text_email('email', 'message', array('name' => $name));
					$mail_properties['html'] = $this->html_email('email', 'message', array('name' => $name));
					$mail_properties['original_message_id'] = $this->message->id->value;

					$message = new mvaccmail($mail_properties);
					if ($message->save()) {
						$emails_sent++;
					}
					else {
						error('Could not queue email');
						$error_found = true;
					}

				}
			}
			if (!empty($texts)) {
				foreach ($texts as $name => $number) {

					$text_properties['Number'] = $number;
					$text_properties['Text'] = $this->message->Message->value;
					$text_properties['original_message_id'] = $this->message->id->value;

					$text = new text_message($text_properties);

					if ($text->save()) {
						$texts_sent++;
					}
					else {
						error('Could not queue text message');
						$error_found = true;
					}
				}
			}
			if (!$error_found && ($texts_sent > 0 || $emails_sent > 0)) {
				$this->message->Sent->value = "Yes";
				$this->message->update();
				$returnable = new stdClass();
				$returnable->text_count = $texts_sent;
				$returnable->email_count = $emails_sent;
				echo json_encode($returnable);
			}
			else if (!$error_found) {
				error('No contacts match the send criteria');
			}
		}

	}

}

?>
