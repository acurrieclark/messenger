<?php

/**
* Message Categories Controller
*/

class messageCategoriesController extends applicationController
{

	function __construct()
	{
		// Use this to control the creation of your application controller
		parent::__construct();
		$this->before_filters = array('must_be_admin' => array('all'));
	}

	function _index() {
		$this->message_categories = message_category::find_all();
	}

	function _show() {
		$this->message_category = message_category::find_by_id($this->id);
	}

	function _new() {
		$this->message_category = new message_category($this->posted);
	}

	function _create() {
		$this->message_category = new message_category($this->posted);
		if ($this->message_category->save()) {
			flash('Message Category created.');
			redirect_to('message_categories');
		}
		else {
			error('There appears to be an error with the message_category.');
			render_action('new');
		}
	}

	function _edit() {
		$this->message_category = message_category::find_by_id($this->id);
	}

	function _update() {
		$this->message_category = new message_category(array_merge(array('id' => $this->id), $this->posted));
		if ($this->message_category->update()) {
			flash('Message Category updated.');
			redirect_to('message_categories');
		}
		else {
			error('There appears to be an error with the message_category.');
			render_action('edit');
		}
	}

		function _delete() {
			$this->message_category = message_category::find_by_id($this->id);
		if ($this->message_category->destroy()) {
			flash('Message Category '. $this->id .' deleted');
			redirect_to('back');
		}
		else {
			error('Message Category could not be deleted');
			redirect_to('back');
		}
	}


}

?>
