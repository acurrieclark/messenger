<?php

// application helper functions go here

function unread_message_count() {
	$count = received_message::count(array('where' => array('read' => "No")));
	if ($count > 0) {
		return ' <span class="label label-info" id="message-count-navbar">'.$count.'</span>';
	}
}

function unsent_message_count() {
	$count = message::count(array('where' => array('Sent' => "No")));
	if ($count > 0) {
		return ' <span class="label label-danger" id="unnaproved-count-navbar">'.$count.'</span>';
	}
}

?>
