
<?php

add_stylesheet('mailboxes');

include_javascript('channel-2');

$sent = message::find_all(array('where' => array('Sent' => 'Yes'), 'order' => array('created_at' => 'DESC')));

$this->set_menu('channel-2');
set('title','Channel 2');
?>
<div class="row" style="height: 100vh;">
	<div id="message-list-container" class="col-md-4 col-sm-5 col-xs-12" style="height: 100%;">
			<ul class="nav nav-tabs" style="margin-top:2px; margin-bottom: -1px">
			    <li role="presentation" class="active"><a href="#all"role="tab" data-toggle="tab" id="sent-button">All</a></li>
			    <!-- <li role="presentation" class="pull-right"><a href="#search"role="tab" data-toggle="tab"><?= icon('search') ?></a></li> -->
			</ul>
		<div class="tab-content message-sidebar">
			<div style="overflow: scroll; height: 100%;" id="all" role="tabpanel" class="tab-pane active">

				<div class="list-group">
				<?php

				if (!empty($sent)) {
					foreach ($sent as $message): ?>
						<a href="#" class="list-group-item message-block" data-message-id="<?= $message->id ?>">
						    <h4 class="list-group-item-heading">
						    	<?= $message->JobName ?></h4>
						    <h6><?= $message->ProductionCompany ?></h6>
						    <p class="list-group-item-text"><?= substr(show_safely($message->Message), 0, 160) ?><?= (strlen($message->Message) > 160) ? '...' : '' ?></p>
						</a>
					<?php endforeach;
				}
				else {
					 ?>
					 <div class="no-messages center-block text-center">
					 	<h3 class="watermarked">No Messages</h3>
					 </div>
					 <?php
				}
					?>
				</div>
			</div>
			<div style="overflow: scroll; height: 100%; margin-top: 10px" id="search" role="tabpanel" class="tab-pane">
				<div class="row">
					<div class="col-md-12">
						<form accept-charset="utf-8" style="padding: 0px 10px">
							<input type="search" class="form-control" placeholder="Search">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="message-display-container" class="col-md-8 col-sm-7 col-xs-12 hidden-xs" style="height: 100%;">
		<div style="height: 100%;">
			<div class="row">
				<div class="col-xs-12 visible-xs-block" id="message-buttons" style="display: none">
					<button id="back-button" type="button" class="btn btn-link pull-left"><?= icon('arrow-left') ?> Back</button>
				</div>
				<div id="system-messages">
					<?php messages() ?>
				</div>
				<div class="center-block text-center col-md-12 col-sm-12" id="select-message-text">
					<h1 class="watermarked">Select Message</h1>
				</div>
				<div class="loading-div" style="display: none" id="message-loader">
					<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Loading</span>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12" id="message-details" style="display: none">
				</div>
			</div>
			<div class="row message-main" style="display: none">
				<div class="col-sm-4">
					<div id="more-message-details" style="display: none">

					</div>
				</div>

				<div id="message-div" class="col-sm-8">

				</div>
			</div>
		</div>
	</div>
</div>
