
<?php

$this->must_be_admin();

include_javascript('inbox');

if (isset($this->get['show'])) {
	 javascript_start();
	  ?>
$(function () {
	  $('a[data-message-id=<?= $this->get['show'] ?>]').trigger('click');
	  $('#messages').animate({
        scrollTop: $("a[data-message-id=<?= $this->get['show'] ?>]").offset().top
    }, 1000);
});

	  <?php
	 javascript_end();
}

add_stylesheet('mailboxes');

$messages = received_message::find_all(array('order' => array('created_at' => 'DESC')));
$this->set_menu('inbox');
set('title','Inbox');
?>
<div class="row" style="height: 100vh;">
	<div id="message-list-container" class="col-md-4 col-sm-5 col-xs-12" style="height: 100%;">
		<div style="overflow: scroll; height: 100%;" class="message-sidebar" id="messages">
			<div class="list-group">
				<?php if (!empty($messages)): ?>
			<?php foreach ($messages as $message): ?>
				<?php
					if ($message->Source->value == "Mobile") {
						$icon = "phone";
						$from = $message->MobileNumber;
					}
					else if ($message->Source->value == "email") {
						$icon = 'envelope';
						$from = $message->email;
					}

					if (isset($message->UserID->value))
						$from = user::find_by_id($message->UserID)->full_Name();

				?>
				<a href="#" class="list-group-item message-block" data-message-id="<?= $message->id ?>">
				    <h4 class="list-group-item-heading">
				    	<span class="badge"><?= icon($icon) ?></span>
				    	<?= ($message->read->value == "No") ? '<span class="label label-info">New</span>' : ''?>
				    	<?= $from ?></h4>
				    <h6><?= $message->created_at->show() ?></h6>
				    <p class="list-group-item-text"><?= substr(show_safely($message->Content), 0, 160) ?><?= (strlen($message->Content) > 160) ? '...' : '' ?></p>
				</a>
			<?php endforeach ?>
					<?php else: ?>
					<div class="no-messages center-block text-center">
					 	<h3 class="watermarked">No Messages</h3>
					 </div>
				<?php endif ?>
			</div>
		</div>
	</div>
	<div id="message-display-container" class="col-md-8 col-sm-7 col-xs-12 hidden-xs" style="height: 100%;">
		<div style="height: 100%;">
			<div class="row">
				<div class="col-sm-3 col-sm-push-9 col-xs-12" id="message-buttons" style="display: none">
					<button id="back-button" type="button" class="btn btn-link visible-xs-block pull-left"><?= icon('arrow-left') ?> Back</button>
					<a href="<?= address('messages', 'new', '', array('received' => '')) ?>" title="" class="btn btn-default pull-right" id="use-message-button">Use Message</a>
				</div>
				<div id="system-messages">
					<?php messages() ?>
				</div>
				<div class="center-block text-center col-md-12 col-sm-12" id="select-message-text">
					<h1 class="watermarked">Select Message</h1>
				</div>
				<div id="inbox-message-loading" class="loading-div" style="display: none">
					<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Loading</span>
				</div>
				<div class="col-sm-9 col-sm-pull-3 col-xs-12" id="message-details" style="display: none">
				</div>
			</div>
			<div class="row message-main" style="display: none">
				<div id="message-div" class="col-md-12">

				</div>
			</div>
		</div>
	</div>
</div>
