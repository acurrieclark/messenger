
<?php

$this->must_be_admin();

add_stylesheet('mailboxes');

include_javascript('outbox');
$unsent = message::find_all(array('where' => array('Sent' => 'No'), 'order' => array('created_at' => 'DESC')));
$sent = message::find_all(array('where' => array('Sent' => 'Yes'), 'order' => array('created_at' => 'DESC')));

$this->set_menu('outbox');
set('title','Outbox');
?>
<div class="row" style="height: 100vh;">
	<div id="message-list-container" class="col-md-4 col-sm-5 col-xs-12" style="height: 100%;">
			<ul class="nav nav-tabs" style="margin-top:2px; margin-bottom: -1px">
				<li role="presentation" class="active"><a href="#unsent"role="tab" data-toggle="tab" id="unsent-button">Unsent <?= label(sizeof($unsent), 'info') ?></a></li>
			    <li role="presentation"><a href="#sent"role="tab" data-toggle="tab" id="sent-button">Sent</a></li>
			    <!-- <li role="presentation" class="pull-right"><a href="#search"role="tab" data-toggle="tab"><?= icon('search') ?></a></li> -->
			    <li role="presentation" class="pull-right"><a href="<?= address('messages', 'new') ?>"><?= icon('envelope') ?> New</a></li>
			</ul>
		<div class="tab-content message-sidebar">
			<div style="overflow: scroll; height: 100%;" id="unsent" role="tabpanel" class="tab-pane active">

				<div class="list-group">
				<?php

				if (!empty($unsent)) {
					$no_messages_style = ' style="display: none"';
					foreach ($unsent as $message): ?>
						<a href="#" class="list-group-item message-block" data-message-id="<?= $message->id ?>">
						    <h4 class="list-group-item-heading">
						    	<?= $message->JobName ?></h4>
						    <h6><?= $message->ProductionCompany ?> <?= ($message->Approved->value == "No") ? '<span class="label label-danger">Unapproved</span>' : '<span class="label label-success">Approved</span>'?></h6>
						    <p class="list-group-item-text"><?= substr(show_safely($message->Message), 0, 160) ?><?= (strlen($message->Message) > 160) ? '...' : '' ?></p>
						</a>
					<?php endforeach;
				}
				else {
					$no_messages_style = '';
				}
					?>
				</div>
				<div id="unsent-no-messages" class="no-messages center-block text-center"<?= $no_messages_style ?>>
					 	<h3 class="watermarked">No Messages</h3>
					 	<a href="<?= ABSOLUTE ?>messages/new" type="button" class="btn btn-sm btn-default"><?= icon('envelope') ?> New Message</a>
				</div>
			</div>
			<div style="overflow: scroll; height: 100%;" id="sent" role="tabpanel" class="tab-pane">
				<div class="loading-div" id="sent-loading-div" style="display: none">
					<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Loading</span>
				</div>
				<div id="sent-no-messages" class="no-messages center-block text-center" style="display: none">
					<h3 class="watermarked">No Messages</h3>
					<a href="<?= ABSOLUTE ?>messages/new" type="button" class="btn btn-sm btn-default"><?= icon('envelope') ?> New Message</a>
				</div>
			</div>
			<div style="overflow: scroll; height: 100%; margin-top: 10px" id="search" role="tabpanel" class="tab-pane">
				<div class="row">
					<div class="col-md-12">
						<form accept-charset="utf-8" style="padding: 0px 10px">
							<input type="search" class="form-control" placeholder="Search">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="message-display-container" class="col-md-8 col-sm-7 col-xs-12 hidden-xs" style="height: 100%;">
		<div style="height: 100%;">
			<div class="row">
				<div class="col-md-4 col-md-push-8 col-sm-12 col-xs-12" id="message-buttons" style="display: none">
					<button id="back-button" type="button" class="btn btn-link visible-xs-block pull-left"><?= icon('arrow-left') ?> Back</button>
					<div class="btn-group pull-right" role="group">
						<a href="" title="" class="btn btn-default" id="edit-button"><?= icon('edit') ?> Edit</a>
						<button type="button" title="Send Message" class="btn btn-default" id="send-button" data-toggle="modal" data-target="#sendModal"><?= icon('send') ?> Send</button>
						<button title="Approve Message" class="btn btn-default" id="approve-button" data-toggle="modal" data-target="#approveModal"><?= icon('check') ?> Approve</button>
					</div>
				</div>
				<div id="system-messages">
					<?php messages() ?>
				</div>
				<div class="center-block text-center col-md-12 col-sm-12" id="select-message-text">
					<h1 class="watermarked">Select Message</h1>
				</div>
				<div class="loading-div" style="display: none" id="message-loader">
					<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Loading</span>
				</div>
				<div class="col-md-8 col-md-pull-4 col-sm-12 col-xs-12" id="message-details" style="display: none">
				</div>
			</div>
			<div class="row message-main" style="display: none">
				<div class="col-sm-4">
					<div id="more-message-details" style="display: none">

					</div>
				</div>

				<div id="message-div" class="col-sm-8">

				</div>
			</div>
		</div>
	</div>
</div>

<!-- send confirmation modal -->

<div class="modal fade" id="sendModal" tabindex="-1" role="dialog" aria-labelledby="sendModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<p class="lead" id="confirm-send-message">Please confirm you wish to send this message.</p>
      	<div id="confirm-send-loader" class="loading-div" style="display: none">
      		<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Please wait</span>
      	</div>
      	<div id="send-information" style="display: none">

      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-send">Cancel</button>
        <button type="button" class="btn btn-success" id="confirm-send">Send</button>
      </div>
    </div>
  </div>
</div>

<!-- approve message modal -->

<div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<p class="lead" id="confirm-approve-message">Please confirm you wish to approve this message.</p>
      	<div id="confirm-approve-loader" class="loading-div" style="display: none">
      		<img src="<?= ABSOLUTE.'img/loading.gif' ?>" alt="Loading Animation" width="40px" height="40px">
					<span class="watermarked">Please wait</span>
      	</div>
      	<div id="approve-information" style="display: none">

      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-approve">Cancel</button>
        <button type="button" class="btn btn-success" id="confirm-approve">Approve</button>
      </div>
    </div>
  </div>
</div>
