<?php

 set('title', "Welcome"); ?>

	<h1>Welcome to Messenger</h1>
	<?php messages(); ?>
	<p class="lead">This is a prototype message distribution service.</p>
	<p>Messages are received either at a dedicated email address or individual mobile number. These are stored in one inbox for reading. Each message can be referenced by an outgoing message again sent by text or email.</p>
	<p>Users signup and choose which category of messages applies to them as well as how they receive them.</p>


