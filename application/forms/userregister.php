<?php

/**
* newUser
*/
class userRegisterForm extends formTemplate
{

	function structure()
	{
		return array(
			array("model" => 'user', 'element' => 'FirstName'),
			array("model" => 'user', 'element' => 'Surname'),

			array("model" => 'user', 'element' => 'Gender'),
			array("model" => 'user', 'element' => 'DateofBirth'),

			array('model' => 'user', 'element' => 'MobileNumber'),

			array('model' => 'user', 'element' => 'email'),
			array("Password", "password", 'confirmation'=>true, 'short_name' => "password"),

			array('model' => 'user', 'element'=>"Role"),
			array('model' => 'user', 'element'=>"message_types"),
			array('model' => 'user', 'element' => 'contact_type')

		);
	}
}


?>
