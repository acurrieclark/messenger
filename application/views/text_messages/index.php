<?php set('title', 'Text Messages List') ?>

<h1>Text Messages List</h1>

<?php messages();

include_javascript('index');

?>

<a class="btn btn-success" href="<?= address('text_messages', 'new') ?>">New Text Message</a>

<?php if (empty($this->text_messages)): ?>
	<h3>No Text Messages to display</h3>
<?php else: ?>

<div class="table-responsive">
	<table class="table table-condensed">

		<thead>
			<tr>
				<?= reset($this->text_messages)->model_table_header(); ?>
				<th>Controls</th>
			</tr>
		</thead>

		<tbody>

<?php

foreach ($this->text_messages as $text_message) {

 ?>

			<tr id="row_<?= $text_message->id ?>">
				<?= $text_message->model_table_row() ?>
				<td>
					<div class="btn-toolbar">
						<a class="btn btn-success btn-xs" href="<?= address('text_messages', 'edit', $text_message->id) ?>">Edit</a>
						<button class="btn btn-danger btn-xs delete-button" id="delete_button_<?= $text_message->id ?>" data-loading-text="Delete">Delete</button>
					</div>
				</td>
			</tr>


<?php } ?>

		</tbody>
	</table>

</div>

<?php endif ?>
