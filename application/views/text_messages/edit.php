<?php set('title', 'Edit Text Message') ?>

<div class="page-header">

  <h1>Text Messages <small>Edit Text Message</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

	messages();

	render_partial('form', array('target' => ABSOLUTE.'text_messages/edit/'.$this->id, 'button' => 'Save'));

