<h3 id="user_details_updated">User Details Updated</h3>
<p class="lead">The details for <?= show_safely($this->user->full_name()) ?> have been updated.</p>
