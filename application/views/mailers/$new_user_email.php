<h3>New User<small> <?= show_safely($this->user->full_name()) ?></small></h3>
<p>A new user has registered and requires verification.</p>
<p>Please <a href="<?= address('members', 'list', '', array('status' => 'unverified')) ?>">login</a> to confirm their details and activate their account.</p>
