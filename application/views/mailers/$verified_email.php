<h3><?php echo show_safely($this->user->full_name()) ?><small> Membership Verified</small></h3>

<p class="lead">Your details have been verified!</p>
<p>You may now <a href="<?= address('login') ?>">login</a>.</p>
