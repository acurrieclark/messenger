<?php set('title', 'Edit Received Message') ?>

<div class="page-header">

  <h1>Received Messages <small>Edit Received Message</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

	messages();

	render_partial('form', array('target' => ABSOLUTE.'received_messages/edit/'.$this->id, 'button' => 'Save'));

