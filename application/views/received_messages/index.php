<?php set('title', 'Received Messages List') ?>

<h1>Received Messages List</h1>

<?php messages();

include_javascript('index');

?>

<a class="btn btn-success" href="<?= address('received_messages', 'new') ?>">New Received Message</a>

<?php if (empty($this->received_messages)): ?>
	<h3>No Received Messages to display</h3>
<?php else: ?>

<div class="table-responsive">
	<table class="table table-condensed">

		<thead>
			<tr>
				<?= reset($this->received_messages)->model_table_header(); ?>
				<th>Controls</th>
			</tr>
		</thead>

		<tbody>

<?php

foreach ($this->received_messages as $received_message) {

 ?>

			<tr id="row_<?= $received_message->id ?>">
				<?= $received_message->model_table_row() ?>
				<td>
					<div class="btn-toolbar">
						<a class="btn btn-success btn-xs" href="<?= address('received_messages', 'edit', $received_message->id) ?>">Edit</a>
						<button class="btn btn-danger btn-xs delete-button" id="delete_button_<?= $received_message->id ?>" data-loading-text="Delete">Delete</button>
					</div>
				</td>
			</tr>


<?php } ?>

		</tbody>
	</table>

</div>

<?php endif ?>
