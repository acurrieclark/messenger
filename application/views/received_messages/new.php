<?php set('title', 'New Received Message') ?>

<div class="page-header">

  <h1>Received Messages <small>New Received Message</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

	messages();

	render_partial('form', array('target' => ABSOLUTE.'received_messages/new', 'button' => 'Save'	));

