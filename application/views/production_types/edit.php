<?php set('title', 'Edit Production Type') ?>

<div class="page-header">

  <h1>Production Types <small>Edit Production Type</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

	messages();

	render_partial('form', array('target' => ABSOLUTE.'production_types/edit/'.$this->id, 'button' => 'Save'));

