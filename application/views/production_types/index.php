<?php set('title', 'Production Types List') ?>

<h1>Production Types List</h1>

<?php messages();

include_javascript('index');

?>

<a class="btn btn-success" href="<?= address('production_types', 'new') ?>">New Production Type</a>

<?php if (empty($this->production_types)): ?>
	<h3>No Production Types to display</h3>
<?php else: ?>

<div class="table-responsive">
	<table class="table table-condensed">

		<thead>
			<tr>
				<?= reset($this->production_types)->model_table_header(); ?>
				<th>Controls</th>
			</tr>
		</thead>

		<tbody>

<?php

foreach ($this->production_types as $production_type) {

 ?>

			<tr id="row_<?= $production_type->id ?>">
				<?= $production_type->model_table_row() ?>
				<td>
					<div class="btn-toolbar">
						<a class="btn btn-success btn-xs" href="<?= address('production_types', 'edit', $production_type->id) ?>">Edit</a>
						<button class="btn btn-danger btn-xs delete-button" id="delete_button_<?= $production_type->id ?>" data-loading-text="Delete">Delete</button>
					</div>
				</td>
			</tr>


<?php } ?>

		</tbody>
	</table>

</div>

<?php endif ?>
