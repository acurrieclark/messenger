<?php set('title', 'Registration Confirmation'); ?>

<div class="page-header">
	<h1>Registration Confirmation</h1>
</div>

<?php messages(); ?>

<p class="lead">
	Thank you for registering. An email has been sent to <?= retrieve_and_remove('signup_email') ?> with your activation details.
</p>

<p>Please follow the instructions to activate your account. Once you have confirmed your email address, we will check your details and let you know when your account is ready to use.</p>

<?php




?>
