<?php
set('title', 'Change Password');
?>
<div class="page-header">
		<h1>Change Password <small><?= $this->user->Name ?></small></h1>
</div>


		<?php
			messages();
		?>

		<div class="row">
			<div class="col-md-4 col-md-offset-1">
				<div id="change_password_block">
					<?php
						$this->form->form(ABSOLUTE.'users/change-password/'.$this->user->id, "Change Password");
					?>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-3">
				<?php render_partial('user_functions', array('active' => 'password')); ?>
			</div>
		</div>


