
function send_confirm_modal_reset() {
    $('#confirm-send-message').show();
    $('#confirm-send-loader').hide();
    $('#send-information').hide();
    $('#confirm-send').show();
    $('#cancel-send').html('Cancel');
  }

  function approve_confirm_modal_reset() {
    $('#confirm-approve-message').show();
    $('#confirm-approve-loader').hide();
    $('#approve-information').hide();
    $('#confirm-approve').show();
    $('#cancel-approve').html('Cancel');
  }

$(function () {

  $("#system-messages").delay(3000).fadeOut("slow");

   $("[rel=tooltip]").tooltip();

   $('#sendModal').on('hidden.bs.modal', function (e) {
      send_confirm_modal_reset();
   });

   $('#approveModal').on('hidden.bs.modal', function (e) {
      approve_confirm_modal_reset();
   });

   // send the message

   $('#confirm-send').click(function () {
    $('#confirm-send-loader').show();
    $('#confirm-send').hide();
    $('#cancel-send').html('Close');
    $('#confirm-send-message').hide();
    message_id = $(this).data('message-id');
    $.getJSON( "<?= ABSOLUTE ?>messages/send/"+message_id, function( data ) {
      $('#send-information').show();
      $('#confirm-send-loader').hide();
      if (data.error) {
        $('#send-information').html('<div class="alert alert-danger">'+data.error+'</div>');
      }
      else {
        $('#send-information').html('<div class="alert alert-success">'+data.text_count+' texts and '+data.email_count+' emails successfuly queued'+'</div>');
        $('[data-message-id='+message_id+']').remove();
        $('.message-block').removeClass('active');
        $('#select-message-text').show();
        $('#message-buttons').hide();
        $("#message-details").html('');
        $('#more-message-details').html();
        $( "#message-div" ).html('');
        $( ".message-main" ).hide();
        var unsent_count = $('#unsent-button').find('.label').html();
        if (+unsent_count == 1) {
          alert(unsent_count);
          $('#unsent-button').find('.label').html('');
          $('#unnaproved-count-navbar').html('');
          $('#unsent-no-messages').show();
        }
        else {
          $('#unsent-button').find('.label').html(+unsent_count - 1+'');
          $('#unnaproved-count-navbar').html(+unsent_count - 1+'');
        }

        $('#message-display-container').addClass('hidden-xs');
        $('#message-list-container').removeClass('hidden-xs');
        $('.message-block').removeClass('active');
        $('#top-navbar').removeClass('hidden-xs');
        $('body').removeClass('no-nav');

      }
    });
   });

  // approve the message

  $('#confirm-approve').click(function () {
    $('#confirm-approve-loader').show();
    $('#confirm-approve').hide();
    $('#cancel-approve').html('Close');
    $('#confirm-approve-message').hide();
    message_id = $(this).data('message-id');
    $.getJSON( "<?= ABSOLUTE ?>messages/approve/"+message_id, function( data ) {
      $('#approve-information').show();
      $('#confirm-approve-loader').hide();
      if (data.error) {
        $('#approve-information').html('<div class="alert alert-danger">'+data.error+'</div>');
      }
      else {
        $('#approve-information').html('<div class="alert alert-success">'+data.message+'</div>');
        $('[data-message-id='+message_id+']').find('.label').removeClass('label-danger').addClass('label-success').html('Approved');
        $('#approve-button').hide();
        $('#send-button').show();
      }
    });
  });

   $(document).on('click', ".message-block", function (e) {

      $('#top-navbar').addClass('hidden-xs');
      $('body').addClass('no-nav');
      $('#message-display-container').removeClass('hidden-xs');
      $('#message-list-container').addClass('hidden-xs');

      $('#select-message-text').hide();
      $('.loading-div').show();
      $('#message-buttons').hide();
      $("#message-details").html('');
      $('#more-message-details').html();
      $( "#message-div" ).html('');
      $( ".message-main" ).hide();


      $(this).parent().find('.message-block').removeClass('active');

      var this_block = this;
      $(this_block).addClass('active');
      var message_id;
      message_id = $(this).data('message-id');

      $.getJSON( "<?= ABSOLUTE ?>messages/show/"+message_id, function( data ) {
        var from, received;
        $('#confirm-send').data('message-id', message_id);
        $('#confirm-approve').data('message-id', message_id);
        if (data.message.Sent == 'No') {
          if (data.message.Approved == 'Yes') {
            $('#send-button').show();
            $('#approve-button').hide();
          }
          else if (data.message.Approved == 'No') {
            $('#send-button').hide();
            $('#approve-button').show();
          }
        }

        $('#message-buttons').show();
        $('#message-details').show();
        $('#more-message-details').show();
        $('.loading-div').hide();
        $('#use-message-button').show();
        $( ".message-main" ).show();

        $("#message-details").html($('<h3>'+data.message.JobName+' <small>'+data.message.ProductionCompany+'</small></h3>'));
        $( "#message-details" ).append($('<h4>'+data.message.created_at+'</h4>'));
        $( "#message-div" ).html($('<div>'+data.message.Message+'</div>'));
        $('#more-message-details').html($('<dl>\
  <dt>Production Type</dt>\
  <dd>'+data.message.ProductionType+"</dd>\
  <dt>Applies to</dt>\
  <dd>"+data.message.Category+'</dd></dl>\
'));
        if (data.message.OriginalMessage) {

          $('#more-message-details').append($('<h4>Original Message</h4><a href="<?= ABSOLUTE ?>inbox/?show='+data.message.OriginalMessage+'" title="" class="btn btn-default btn-sm" id="show-origninal-button">Show Original Message</a>'));
        }
        href = $('#use-message-button').attr('href');
        $('#edit-button').attr('href', "<?= ABSOLUTE ?>messages/edit/"+data.message.id);
      });

      // if ($(this_block).find('span.label').length) {
      //   $.get("<?= ABSOLUTE ?>messages/mark-as-read/"+message_id, function( data ) {
      //     if (data.message == "Success") {
      //       $(this_block).find('span.label').remove();
      //       var message_count = parseInt($('#message-count-navbar').text(), 10) - 1;
      //       if (message_count < 1) $('#message-count-navbar').remove();
      //       else $('#message-count-navbar').html(""+message_count);
      //     }
      //   });
      // }

	});

  $('#unsent-button').on('shown.bs.tab', function (e) {
    $('#edit-button').show();
    $('.message-block').removeClass('active');
    $('#select-message-text').show();
    $('#sent-loading-div').show();
    $('#message-buttons').hide();
    $("#message-details").html('');
    $('#more-message-details').html();
    $( "#message-div" ).html('');
    $( ".message-main" ).hide();
  });

  $('#sent-button').on('shown.bs.tab', function (e) {
    $('#send-button').hide();
    $('#approve-button').hide();
    $('#edit-button').hide();
    $('#sent-no-messages').hide();
    $('.message-block').removeClass('active');
    $('#select-message-text').show();
    $('#sent-loading-div').show();
    $('#message-buttons').hide();
      $("#message-details").html('');
      $('#more-message-details').html();
      $( "#message-div" ).html('');
      $( ".message-main" ).hide();

    $.getJSON( "<?= ABSOLUTE ?>messages/sent/", function( data ) {
      $('#sent-loading-div').hide();
      if (data) {
        $('#sent').html('');
        var new_list_item = $('<div class="list-group"></div>');
        $('#sent-no-messages').hide();
        $.each(data, function(index, message) {
          var label;
          if (message.Approved == 'Yes') label = '<span class="label label-success">Approved</span>';
          else label = '<span class="label label-danger">Unapproved</span>';
          $(new_list_item).append($('<a href="#" class="list-group-item message-block" data-message-id="'+index+'">\
    <h4 class="list-group-item-heading">'+message.JobName+'</h4>\
              <h6>'+message.ProductionCompany+' '+label+'</h6>\
              <p class="list-group-item-text">'+message.Message+'</p>\
  </a>'));
          $('#sent').append($(new_list_item));
        });

      }
      else {
        $('#sent-no-messages').show();
      }
    });
  });

  $('#back-button').click(function(event) {
    $('#message-display-container').addClass('hidden-xs');
      $('#message-list-container').removeClass('hidden-xs');
      $('.message-block').removeClass('active');
      $('#top-navbar').removeClass('hidden-xs');
      $('body').removeClass('no-nav');
  });

 });
