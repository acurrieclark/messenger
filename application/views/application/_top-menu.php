
<?php

$active = ' class="active"';

$menu_select['home'] = '';
$menu_select['inbox'] = '';
$menu_select['users'] = '';
$menu_select['channel-2'] = '';

if (!$variables['menu-select'])
	$menu_select['home'] = $active;
else {
	$menu_select[$variables['menu-select']] = $active;
}

?>


<nav class="navbar navbar-default navbar-fixed-top" id="top-navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= ABSOLUTE ?>">Messenger</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php if (logged_in()): ?>
          <li<?= $menu_select['channel-2'] ?>><a href="<?= ABSOLUTE ?>channel-2">Channel 2</a></li>
        <?php endif ?>
        <?php if (is_admin()): ?>
          <li<?= $menu_select['inbox'] ?>><a href="<?= ABSOLUTE ?>inbox">Inbox<?= unread_message_count() ?></a></li>
          <li<?= $menu_select['outbox'] ?>><a href="<?= ABSOLUTE ?>outbox">Outbox<?= unsent_message_count() ?></a></li>
          <li<?= $menu_select['users'] ?>><a href="<?= ABSOLUTE ?>users/list">Users</a></li>
        <?php endif ?>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php if (logged_in()): ?>
        <li class="dropdown"<?= $menu_select['users'] ?>>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?= icon('user')." ".current_user()->full_name() ?> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?= address('users', 'edit', current_user()->id) ?>"><?= icon('edit') ?> Edit Your Profile</a></li>
            <li class="divider"></li>
            <li><a href="<?= ABSOLUTE ?>logout"><?= icon('log-out') ?> Logout</a></li>
          </ul>
        </li>

        <?php else: ?>
        <li><a href="<?= ABSOLUTE ?>login">Login</a></li>
        <?php endif ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

