$(function () {

  $("#system-messages").delay(3000).fadeOut("slow");

   $("[rel=tooltip]").tooltip();


   $(document).on('click', ".message-block", function (e) {
      $('#top-navbar').addClass('hidden-xs');
      $('body').addClass('no-nav');
      $('#message-display-container').removeClass('hidden-xs');
      $('#message-list-container').addClass('hidden-xs');
      $('#select-message-text').hide();
      $('.loading-div').show();
      $('#message-buttons').hide();
      $("#message-details").html('');
      $('#more-message-details').html();
      $( "#message-div" ).html('');
      $( ".message-main" ).hide();


      $(this).parent().find('.message-block').removeClass('active');

      var this_block = this;
      $(this_block).addClass('active');
      var message_id;
      message_id = $(this).data('message-id');

      $.getJSON( "<?= ABSOLUTE ?>messages/show/"+message_id, function( data ) {
        var from, received;
        $('#confirm-send').data('message-id', message_id);
        $('#confirm-approve').data('message-id', message_id);

        $('#message-buttons').show();
        $('#message-details').show();
        $('#more-message-details').show();
        $('.loading-div').hide();
        $('#use-message-button').show();
        $( ".message-main" ).show();

        $("#message-details").html($('<h3>'+data.message.JobName+' <small>'+data.message.ProductionCompany+'</small></h3>'));
        $( "#message-details" ).append($('<h4>'+data.message.created_at+'</h4>'));
        $( "#message-div" ).html($('<div>'+data.message.Message+'</div>'));
        $('#more-message-details').html($('<dl>\
  <dt>Production Type</dt>\
  <dd>'+data.message.ProductionType+"</dd>\
  <dt>Applies to</dt>\
  <dd>"+data.message.Category+'</dd></dl>\
'));
        if (data.message.OriginalMessage) {

          $('#more-message-details').append($('<h4>Original Message</h4><a href="<?= ABSOLUTE ?>inbox/?show='+data.message.OriginalMessage+'" title="" class="btn btn-default btn-sm" id="show-origninal-button">Show Original Message</a>'));
        }
      });

	});

  $('#sent-button').on('shown.bs.tab', function (e) {
    $('#send-button').hide();
    $('#approve-button').hide();
    $('#edit-button').hide();
    $('#sent-no-messages').hide();
    $('.message-block').removeClass('active');
    $('#select-message-text').show();
    $('#sent-loading-div').show();
    $('#message-buttons').hide();
      $("#message-details").html('');
      $('#more-message-details').html();
      $( "#message-div" ).html('');
      $( ".message-main" ).hide();

    $.getJSON( "<?= ABSOLUTE ?>messages/sent/", function( data ) {
      $('#sent-loading-div').hide();
      if (data) {
        $('#sent').html('');
        var new_list_item = $('<div class="list-group"></div>');
        $('#sent-no-messages').hide();
        $.each(data, function(index, message) {
          var label;
          if (message.Approved == 'Yes') label = '<span class="label label-success">Approved</span>';
          else label = '<span class="label label-danger">Unapproved</span>';
          $(new_list_item).append($('<a href="#" class="list-group-item message-block" data-message-id="'+index+'">\
    <h4 class="list-group-item-heading">'+message.JobName+'</h4>\
              <h6>'+message.ProductionCompany+' '+label+'</h6>\
              <p class="list-group-item-text">'+message.Message+'</p>\
  </a>'));
          $('#sent').append($(new_list_item));
        });

      }
      else {
        $('#sent-no-messages').show();
      }
    });
  });

  $('#back-button').click(function(event) {
    $('#message-display-container').addClass('hidden-xs');
      $('#message-list-container').removeClass('hidden-xs');
      $('.message-block').removeClass('active');
      $('#top-navbar').removeClass('hidden-xs');
      $('body').removeClass('no-nav');
  });

 });
