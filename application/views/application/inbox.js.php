
$(function () {
  $("#system-messages").delay(3000).fadeOut("slow");
   $("[rel=tooltip]").tooltip();
   $(".message-block").click(function () {
      $('#top-navbar').addClass('hidden-xs');
      $('body').addClass('no-nav');
      $('#message-display-container').removeClass('hidden-xs');
      $('#message-list-container').addClass('hidden-xs');
      $('#select-message-text').remove();
      $('.loading-div').show();
      $('#message-buttons').hide();
      $("#message-details").html('');
      $( "#message-div" ).html('');
      $( ".message-main" ).hide();

      $(this).parent().find('.message-block').removeClass('active');

      var this_block = this;
      $(this_block).addClass('active');
      var message_id;
      message_id = $(this).data('message-id');

      $.getJSON( "<?= ABSOLUTE ?>received_messages/show/"+message_id, function( data ) {
        var from, received;

        $('#message-buttons').show();
        $('#message-details').show();

        $('.loading-div').hide();
        $('#use-message-button').show();
        $(".message-main").show();

        if (data.user) {
          from = data.user.FirstName+" "+data.user.Surname+" ";
        }
        else from = '';

        if (data.message.Source == 'email') from += "<small>"+data.message.email+"</small>";
        else if (data.message.Source == 'Mobile') from += "<small>"+data.message.MobileNumber+"</small>";

        $("#message-details").html($('<h3>'+from+'</h3>'));
        $( "#message-details" ).append($('<p><strong>Received:</strong> '+data.message.created_at+'</p>'));
        $( "#message-div" ).html($('<div>'+$.nl2br(data.message.Content)+'</div>'));
        href = $('#use-message-button').attr('href');
        split_href = href.split('?');
        $('#use-message-button').attr('href', split_href[0]+'?'+'received='+data.message.id);
      });

      if ($(this_block).find('span.label').length) {
        $.get("<?= ABSOLUTE ?>received_messages/mark-as-read/"+message_id, function( data ) {
          if (data.message == "Success") {
            $(this_block).find('span.label').remove();
            var message_count = parseInt($('#message-count-navbar').text(), 10) - 1;
            if (message_count < 1) $('#message-count-navbar').remove();
            else $('#message-count-navbar').html(""+message_count);
          }
        });
      }

	});

  $('#back-button').click(function(event) {
    $('#message-display-container').addClass('hidden-xs');
      $('#message-list-container').removeClass('hidden-xs');
      $('.message-block').removeClass('active');
      $('#top-navbar').removeClass('hidden-xs');
      $('body').removeClass('no-nav');
  });

 });
