<?php set('title', 'Message Categories List') ?>

<h1>Message Categories List</h1>

<?php messages();

include_javascript('index');

?>

<a class="btn btn-success" href="<?= address('message_categories', 'new') ?>">New Message Category</a>

<?php if (empty($this->message_categories)): ?>
	<h3>No Message Categories to display</h3>
<?php else: ?>

<div class="table-responsive">
	<table class="table table-condensed">

		<thead>
			<tr>
				<?= reset($this->message_categories)->model_table_header(); ?>
				<th>Controls</th>
			</tr>
		</thead>

		<tbody>

<?php

foreach ($this->message_categories as $message_category) {

 ?>

			<tr id="row_<?= $message_category->id ?>">
				<?= $message_category->model_table_row() ?>
				<td>
					<div class="btn-toolbar">
						<a class="btn btn-success btn-xs" href="<?= address('message_categories', 'edit', $message_category->id) ?>">Edit</a>
						<button class="btn btn-danger btn-xs delete-button" id="delete_button_<?= $message_category->id ?>" data-loading-text="Delete">Delete</button>
					</div>
				</td>
			</tr>


<?php } ?>

		</tbody>
	</table>

</div>

<?php endif ?>
