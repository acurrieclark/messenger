<?php set('title', 'New Message Category') ?>

<div class="page-header">

  <h1>Message Categories <small>New Message Category</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

	messages();

	render_partial('form', array('target' => ABSOLUTE.'message_categories/new', 'button' => 'Save'	));

