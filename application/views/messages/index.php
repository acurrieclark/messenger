<?php set('title', 'Messages List') ?>

<h1>Messages List</h1>

<?php messages();

include_javascript('index');

?>

<a class="btn btn-success" href="<?= address('messages', 'new') ?>">New Message</a>

<?php if (empty($this->messages)): ?>
	<h3>No Messages to display</h3>
<?php else: ?>

<div class="table-responsive">
	<table class="table table-condensed">

		<thead>
			<tr>
				<?= reset($this->messages)->model_table_header(); ?>
				<th>Controls</th>
			</tr>
		</thead>

		<tbody>

<?php

foreach ($this->messages as $message) {

 ?>

			<tr id="row_<?= $message->id ?>">
				<?= $message->model_table_row() ?>
				<td>
					<div class="btn-toolbar">
						<a class="btn btn-success btn-xs" href="<?= address('messages', 'edit', $message->id) ?>">Edit</a>
						<button class="btn btn-danger btn-xs delete-button" id="delete_button_<?= $message->id ?>" data-loading-text="Delete">Delete</button>
					</div>
				</td>
			</tr>


<?php } ?>

		</tbody>
	</table>

</div>

<?php endif ?>
