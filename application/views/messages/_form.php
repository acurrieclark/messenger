<?php

add_javascript('sms-counter-master/sms_counter');

$this->message->form_header($variables['target']);

	?>

<div class="row">
	<div class="col-md-3">
		<?= $this->message->form_entry('JobName') ?>
		<?= $this->message->form_entry('ProductionCompany') ?>
	</div>
	<div class="col-md-6">
		<?= $this->message->form_entry('Message') ?>

	<div id="sms-counter">

		<p><span class="length label label-default"></span> Characters <span class="messages label label-info"></span> SMS Messages</p>
	</div>

<?php
	javascript_start();
	 ?>
		$('#Message_message').countSms('#sms-counter');
	<?php
	javascript_end(); ?>

		<?php if (!empty($this->received)): ?>
			<div class="original-message">
				<h4>Original Message <small><?= link_to('Show', 'inbox', '', '', [], ['show' => $this->received->id]) ?></small></h4>
				<?php if (isset($this->received->Subject->value)): ?>
					<h5><?= $this->received->Subject ?></h5>
				<?php endif ?>
				<p><?= $this->received->Content ?></p>
			</div>
		<?php endif ?>

	</div>
	<div class="col-md-3">
		<?= $this->message->form_entry('Category') ?>
		<?= $this->message->form_entry('ProductionType') ?>
	</div>
</div>

<?=

	$this->message->Sent->input_hidden();
	$this->message->Approved->input_hidden();
	include_security();
	 ?>

	 <div class="form-footer">

		<?= $this->message->form_button($variables['button']); ?>

	 </div>

	 <?php

	$this->message->form_footer();
