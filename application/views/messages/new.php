<?php set('title', 'New Message') ?>

<div class="page-header">

  <h1>Messages <small>New Message</small></h1>

  <div class="btn-toolbar right-button-bar hidden-xs">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>

<?php

	messages();

	render_partial('form', array('target' => address('messages', 'new', '', $this->get), 'button' => 'Save'));

