<?php set('title', 'Edit Message') ?>

<div class="page-header">

  <h1>Messages <small>Edit Message</small></h1>

  <div class="btn-toolbar">
  	<?php remote_form_buttons('Save', true); ?>
  </div>

</div>


<?=

messages();

render_partial('form', array('target' => address('messages', 'edit', $this->message->id), 'button' => 'Save'));

